# YAT TIN CHOW, TIANYU WU, AND WOTAO YIN – CYCLIC COORDINATE UPDATE ALGORITHMS FOR FIXED-POINT PROBLEMS: ANALYSIS AND APPLICATIONS
### Zusatzprojekt ,,Einführung in Mathematische Bildverarbeitung” im SS 2019

## Zusammenfassung

In ihrem paper [1] untersuchen die Autoren das Verhalten von Fixpunktiterationen unter
zyklischen Koordinaten-Updates. Mit gewissen Voraussetzungen an den Operator und die
Schrittweite, wird Konvergenz der Fixpunktiteration unter Koordinaten-Updates gezeigt.
Schließlich werden die Resultate numerisch bestätigt und mit vollständigen Updates
verglichen.

![Epochen](results/epochs.png)

Figure 1: Konvergenzverhalten verschiedener Koordinaten-Updates.

![Original](results/phantom_truth.png)
![cyclic](results/phantom_cyclic.png)
![full](results/phantom_full.png)

Figure 2: Von links nach rechts: originales Bild, zyklische Koordinaten-Updates
(100 Epochen), vollständige Updates (100 Epochen).

\[1\] Yat Tin Chow, Tianyu Wu, and Wotao Yin. Cyclic Coordinate Update Algorithms
for Fixed-Point Problems: Analysis and Applications. arXiv:1611.02456, Nov 2016. 