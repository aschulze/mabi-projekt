\begin{abstract}
  In ihrem paper \cite{chow2016} untersuchen die Autoren das Verhalten von Fixpunktiterationen
  unter zyklischen Koordinaten-Updates. Mit gewissen Voraussetzungen an den Operator und
  die Schrittweite, wird Konvergenz der Fixpunktiteration unter Koordinaten-Updates gezeigt.
  Schließlich werden die Resultate numerisch bestätigt und mit vollständigen Updates verglichen.
\end{abstract}

\section{Motivation und Übersicht}

Koordinaten-Updates sind Methoden um große Probleme in kleinere Teilprobleme zu zerlegen.
Sie können auf lineare und nicht-lineare, glatte und nicht-glatte Funktionen, und konvexe
und nicht-konvexe Probleme angewandt werden. Ein Spezialfall ist beispielsweise das
Gauß-Seidel-Verfahren, aber auch bei Optimierungsprobleme findet es Anwendung.
Besonderes Interesses wurden diesen Methoden in den letzten Jahren zuteil,
da daten-zentrische Anwendungen, wie Bildverarbeitung, skalierbare Algorithmen benötigen
um hoch-dimensionale Daten verarbeiten zu können.
\\
Für eine sinnvolle Anwendung sollte die Kosten zur Berechnung aller Teilprobleme einer Iteration,
nicht die Kosten des vollen Updates überschreiten (``computationally worthy'').
\\
Bei einer sequentiellen Bearbeitung der Teilprobleme können verschiedene Reihenfolgen
gewählt werden. Dazu zählen zyklische, zufallsverteilte oder ``greedy'' Algorithmen.
Sequentielle Algorithmen wurden bereits vielfach angewandt, beispielsweise auf
ADMM \cite{GABAY1976} und Douglas-Rachford-Splitting \cite{douglas1956}.
\\
Ebenso kann die Berechnung der Updates parallel geschehen.
Zu disen Algorithmen gehört das sync-parallel (Jacobi) Update.
Hierbei findet eine Synchronisierung nach dem Update aller Koordinaten
der gleichen Iteration statt.
Wird die Synchronisation ausgelassen, nennt man dies async-parallel Update.
Hierbei kann es jedoch zu Koordinaten-Updates mit veralteten Ergebnissen kommen,
da schwächere Garantien für das Laden und Speichern von Werten bestehen.
In der Praxis sind solche Algorithmen sehr schnell. Jedoch ist das
Konvergenzverhalten mathematisch schwierig zu analysieren \cite{Peng_2016}.

\subsection{Problemformulierung}

\paragraph{Kernaussage.}

Bisher wurden Koordinaten-Updates nur für einzelne Algorithmen untersucht, beispielsweise
für coordinate descent \cite{nutini2015coordinate}. Entsprechend gibt es viele Problemklassen
für die die Konvergenz nicht bekannt ist, wie TV-Denoising. Oft lassen sich jedoch
Fixpunktiterationen für diese Probleme finden, zum Beispiel durch Primal-Dual-Splitting.
Deshalb erarbeiten die Autoren Aussagen zum Konvergenzverhalten von Fixpunktiterationen,
wobei sie sich auf nicht-expansive Operatoren beschränken.
In früheren Publikation konnte bereits gezeigt werden, dass zyklische Koordinaten-Updates
in der Anwendung effizienter sind als zufallsbestimmte Koordinaten-Updates.
Somit liegt der Fokus des papers auf dieser Methode \cite{douglas1956}.
\\
Es nicht trivial Probleme zu finden, die ``computationally worthy'' sind.
Siehe \cite{Peng_2016} für eine Übersicht verschiedener Beispiele.
Als Gegenbeispiel, welches nicht ``computationally worthy'' ist, lässt sich das
Newton-Verfahren nennen \cite{Peng_2016} Seite 2.
Aus diesem Grund erarbeiten die Autoren noch einen expliziten Algorithmus für das TV-Denoising,
basierend auf Primal-Dual-Splitting. Angewandt wird dies schließlich auf die
Rekonstruktion von CT-Bildern, um dann die Überlegenheit von zyklischen Updates gegenüber
vollständigen Updates in der Anwendung zu zeigen.

\section{Lösungsansatz}

Sei $\mathcal{H} = \mathcal{H}_1 \times \dots \times \mathcal{H}_m$ ein beliebiger
Hilbert-Raum, $x \in \mathcal{H}$ und
$S : \mathcal{H} \xrightarrow{} \mathcal{H}$ ein Operator, sodass
\begin{equation}
  Sx = 0.
  \label{eq:root}
\end{equation}
Die Menge aller Lösungen bezeichnen wir mit
\begin{equation}
  zer(S) := \{x \in \mathcal{H} | Sx = 0\}.
\end{equation}
Äquivalent zu \ref{eq:root} ist die Bestimmung eines Fixpunktes von $T = (I - S)$.
Wir setzen voraus, dass ein solcher Fixpunkt exisitiert und dass $(I - S)$ \textbf{nicht-expansiv} ist,
\begin{equation}
  ||(I-S)x - (I-S)y|| \leq ||x-y|| \,\forall x,y \in \mathcal{H}\,.
  \label{eq:nonexpansive}
\end{equation}
$||\cdot|| = \sqrt{\langle \cdot,\cdot \rangle } $ bezeichnet, die vom inneren Produkt des
Hilbert-Raums $\mathcal{H}$ induzierte Norm.

Ein \textbf{Koordinaten-Update} ist von der Form

\begin{equation}
  x_{1}^{k+1} = x_{i}^{k} - \alpha(x^{k} - S(x^{k})_i)\,, 
  \label{eq:coord_upd}
\end{equation}

mit Schrittweite $\alpha$ und $i \in [m]$. Ist die Komplexität von \ref{eq:coord_upd}
kleiner oder gleich $\frac{1}{m}$ mal der Komplexität des vollständigen Updates, dann
nennt man die Koordinaten-Updates \textbf{``computationally worthy''} und den Operator $S$
\textbf{Coordinate Friendly (CF)}.
\\
Der Algorithmus zur Berechnung von Koordinaten-Updates ist nun wie folgt definiert,
wobei die äußere Schleife als Epoche bezeichnet wird:
\begin{algorithm}
  \SetAlgoLined
  \KwData{$x^0 \in \mathcal{H}$}
  \For{$k = 1,2,\dots$}{
    Sei $(i_1, i_2, \dots, 1_m)$ eine Permutation von $[m]$\;
    (entweder keine Permuation, zufallsverteilt(shuffling) oder greedy)\;
    Wähle $\alpha_k > 0$\;
    Initialisiere $y^0 \leftarrow x^{k-1}$\;
    \For{$j = 1,2,\dots,m$}{
      $y^{j} \leftarrow y^{j-1} - \alpha_k S_{i_j}(y^{j-1})$\;
    }
    $x^k \leftarrow y^m$\;
  }
  \caption{Zyklisches Koordinaten-Update}
  \label{alg:cyclic}
\end{algorithm}

Der Operator zum vollständigen Update und zum zyklischen Koordinaten-Update sind wie
folgt definiert:

\begin{equation}
  T^{\alpha} := I - \alpha S\,,
\end{equation}

\begin{equation}
  E^{\alpha} := (I - \alpha S_{m}) (I - \alpha S_{m-1}) \cdots (I - \alpha S_{1})\,.
\end{equation}

Um die Konvergenz von Algorithmus \ref{alg:cyclic} zu beweisen, wird
gezeigt, dass die Differenz der Operatoren $T^{\alpha}$ und $E^{\alpha}$
beschränkt ist. Dieser Ansatz unterscheidet sich stark von den bisherigen
Konvergenzanalysen. Zum Beispiel wird zur Konvergenz des zyklischen coordinate
descent der Funktionswert der Zielfunktion minimiert \cite{nutini2015coordinate},
was für Fixpunktiterationen nicht möglich ist.
\\
Die Ergebnisse des frameworks werden schließlich auf folgendes Problem
angewendet:

\begin{equation}
  \underset{x \in \mathcal{H}}{\text{minimize}}\,
      g(x) + h(x) + f(Ax)\,,
\end{equation}

mit $g: \mathcal{H} \rightarrow \mathcal{R}$ differenzierbar, konvex,
$f,h: \mathcal{H} \rightarrow \mathcal{R} \cup \{\infty\}$ konvex und
$A \in L(\mathcal{H}, \mathcal{G})$ ein linearer Operator.

Primal-Dual-Splitting liefert folgende Fixpunktiteration mit Dualvariablen
$s \in \mathcal{G}$:

\begin{flalign}
    x^{k+1} &= \text{prox}_{\eta h}\, (x^k - \eta(\nabla g(x^k) + A^{\top}(s^k))) &\label{eq:prox_it}\\
    s^{k+1} &= \text{prox}_{\gamma f^{\ast}}\, (s^k - \gamma A(2x^{k+1} - x^k)) &\notag
\end{flalign}

\ref{eq:prox_it} lässt sich schreiben als $z^{k+1}=Tz$, mit
$z:= \begin{pmatrix} x \\ s \end{pmatrix}$.
Angenommen es gilt nun
\begin{equation*}
  s = (s_1, s_2, ..., s_p) \in
  \mathcal{G}_1 \times \mathcal{G}_2 \times \cdots \times \mathcal{G}_p
  = \mathcal{G},
\end{equation*}

dann kann Algorithmus \ref{alg:cyclic} auf $T$ angewendet werden.
In jeder Iteration wird entweder eine Koordinate von $x$ oder
$s$ aktualisiert.
Es lässt sich zeigen, das $T$ unter weiteren Voraussetzungen nicht-expansiv
ist. Damit sichert Theorem \ref{thm:konv} aus dem nächsten Abschnitt die
Konvergenz des Koordinaten-Updates.
\\
Der Ansatz ist ``computationally worthy'', wenn ein Koordinaten-Update die
Komplexität $\mathcal{O}(\frac{1}{m+p})$ hat. Um das zu erreichen wird
die ``caching''-Technik verwendet, bei der Zwischenwerte gespeichert werden.
Die exakte Implementierung hängt von der Anwendung ab.
Eine präzisere Formulierung folgt in Kapitel \ref{sec:num}.

\section{Theoretische Eigenschaften}
\label{sec:theo}

Es folgt eine Zusammenfassung der wichtigsten Konvergenzresultate
der Autoren. Das umfasst die Konvergenz des
Koordinaten-Updates bei einer absteigenden Folge von Schrittweiten
und bei konstanter Schrittweite, aber mit leicht verstärkten
Anforderungen an den Operator $S$.

\begin{theorem}
  Algorithmus \ref{alg:cyclic} benutze die Folge von Schrittweiten
  \begin{equation}
    \alpha_k = \frac{1}{k^{\frac{1}{2}}}.
  \end{equation}
  Falls $zer(S)=\emptyset$ und $(I-S)$ nicht-expansiv ist, dann erzeugt
  Algorithmus \ref{alg:cyclic} eine schwach konvergente Folge
  $x^k \rightharpoonup x^{\ast}$ für ein $x^{\ast} \in zer(S)$.
  \label{thm:konv}
\end{theorem}

\begin{corollary}
  Es gilt
  \begin{equation*}
    \underset{j \leq k}{\text{min}}\{||Sx^j||^2\} = o(\frac{1}{\sqrt{k}})\,.
  \end{equation*}
\end{corollary}

\begin{definition}{Quasi-$\mu$-stark monoton}
  Ein Operator $S: \mathcal{H} \rightarrow \mathcal{G}$ heißt
  \textit{quasi-$\mu$-stark monoton}, falls ein $\mu>0$ exisitiert mit
  \begin{equation}
    \langle Sx, x-x^{\ast} \rangle \geq \mu ||x-x^{\ast}||^2,\;
    \forall x^{\ast} \in \text{zer}(S), x \in \mathcal{H}.
  \end{equation}
\end{definition}

Es ist $(I-S)$ nicht-expansiv genau dann, wenn $\frac{1}{2}$-kokoerziv ist und damit
ist $S$, sowie $S_i$ 2-Lipschitz. Im Weiteren sei

\begin{equation}
  L := \underset{i}{\text{max}}\, L_i \leq 2.
  \label{eq:lip}
\end{equation}

\begin{theorem}
  Sei $zer(S)=\emptyset$, $(I-S)$ nicht-expansiv und quasi-$\mu$-stark-monoton.
  Dann erzeugt Algorithmus \ref{alg:cyclic}, mit der festen Schrittweite
  \begin{equation*}
    \alpha = \text{min} \Bigg\{ \frac{1}{4mL}, \frac{\mu}{4\sqrt{2}mL},
                                \frac{2mL}{17mL+2\mu^2} \Bigg\}\,,
  \end{equation*}
  eine konvergente Folge $x^k \rightarrow x^{\ast}$ für ein $x^{\ast} \in zer(S)$.
  \label{eq:step_fix}
\end{theorem}

\section{Numerisches Verfahren}
\label{sec:num}

\paragraph{Algorithmus.}

Im Folgenden betrachten wir die diskretisierte Form des anisotropischen
L1-TV-Denoising zur Rekonstruktion von MRT-Bildern,

\begin{equation}
	\underset{x}{\text{minimize}} \,\lambda ||x||_{TV} + \frac{1}{2} ||Ax-b||^2\,,
\end{equation}

wobei $x \in \mathcal{R}^n$ das unbekannte Bild ist, $A \in \mathcal{R}^{m\times n}$
die Transformationsmatrix
und $b$ die Messungen. $\nabla$ sei der diskrete Bildgradient, mit $\nabla x =$
$(\nabla_1^h x, \nabla_1^v x, \dots, \nabla_n^h x, \nabla_n^v x,)^{\top}$,
dann lässt sich die anistrope TV-Seminorm schreiben als

\begin{equation}
	||x||_{TV} =  ||\nabla x||_1 = \sum_{i} \sqrt{|\nabla_{i}^{h}x| + |\nabla_{i}^{v}x|}\,.
\end{equation}

Wir erhalten folgende Fixpunktiteration mit Dualvariablen $s, t$
(für eine Herleitung siehe \cite{Peng_2016} Seite 35):

\begin{equation}
  x^{k+1} = x^{k} - \eta (\nabla^{\top}s^{k} + A^{\top}t^{k})\,,
\end{equation}

\begin{equation}
  s^{k+1} = \text{proj}_{||\cdot||_{\infty} \leq \lambda}\,
                (s^{k} + \gamma\nabla(x^{k} - 2\eta(\nabla^{\top}s^{k} + A^{\top}t^{k})))\,,
\end{equation}

\begin{equation}
  t^{k+1} = \frac{1}{1+\gamma} (t^{k} +
                                \gamma A(x^{k} - 2\eta(\nabla^{\top}s^{k} + A^{\top}t^{k})) -
                                \gamma b)\,.
\end{equation}

Für das Koordinaten-Update werden die Variablen $x, s, t$ entsprechend der
Spaltenzahl des Bildes $x$ eingeteilt. Ein Koordinaten-Update umfasst dabei
alle drei Variablen, im Gegensatz zum Algorithmus in \cite{Peng_2016}
Seite 36.
\\
Die ``caching''-Technik sieht hier wie folgt aus. $\nabla^{\top}s^k$ und
$A^{\top}t^k$ werden gespeichert und nach dem Update von $x,s,t$
durch $\nabla^{\top}s^{k+1} = \nabla^{\top}s^k + \nabla_{:,i}(s^{k+1} - s^k)$,
$A^{\top}t^{k+1} = A^{\top}t^k + A_{:,i}(t^{k+1} - t^k)$ aktualisiert.

\paragraph{Voraussetzungen.}

Vom Anwender können die Schrittweite $\alpha$ des Koordinaten-Updates,
die Schrittweiten $\eta, \gamma$ vom Primal-Dual-Splitting und
der Denoising-Parameter $\lambda$ gesetzt werden.
$\alpha$ hängt, wie in \ref{eq:step_fix} gezeigt, von der Anzahl
der Blöcke ab. Jedoch erwähnen die Autoren, dass in allen Experimenten
stets $\alpha=1$ funktioniert hat.
$\eta, \gamma$ sind von der Samplingmatrix $A$ abhängig und können
über das Theorem 2.7 aus der Vorlesung bestimmt werden.
Im Rahmen des Projektes war dies jedoch nicht möglich, da $A$ vom MRI-Paket
als Funktionszeiger bereitgestellt wird.
Wie im Kontext von Defintion \ref{eq:lip} erwähnt, erlauben die Koordinaten-Updates
eine höhere Schrittweite.
$\lambda$ bestimmt den Grad an blurring und ist abhängig vom Bild und dessen noise.

\paragraph{Eigenschaften.}

Das Konvergenzverhalten wurde bereits in Kapitel \ref{sec:num} dokumentiert, wobei
auch erwähnt wurde, dass die Koordinaten-Updates ``computationally worthy'' sind.

\section{Ergebnisse}

\subsection{Total Variation Image Processing}

\paragraph{Ergebnisse.}

\begin{figure}[h]
	\includegraphics[width=\columnwidth]{06MRI/epochs.png}
  \caption{Konvergenzverhalten relativ zur Anzahl an Epochen.
           Die Parameter sind $\alpha=1, \eta=0.5, \gamma=0.5, \lambda=0.01$.
           Die Koordinaten-Updates konvergieren schneller als das vollständige Update.
           Das zyklische Update ist bis zur 5. Epoche die schnellste Methode,
           so wie in den Experimenten von \cite{chow2016} gesehen.
           Danach konvergieren cyclic, cyclic shuffle und random nahezu identisch.}
	\label{fig:epochs}
\end{figure}

\begin{figure}[h]
  \begin{subfigure}{.25\textwidth}
    \centering
    \includegraphics[width=.8\linewidth]{06MRI/phantom_truth.png}
    \caption{Referenzbild}
    \label{fig:sfig1}
  \end{subfigure}%
  \begin{subfigure}{.25\textwidth}
    \centering
    \includegraphics[width=.8\linewidth]{06MRI/phantom_full.png}
    \caption{Vollständige Updates}
    \label{fig:sfig2}
  \end{subfigure}
  \begin{subfigure}{.25\textwidth}
    \centering
    \includegraphics[width=.8\linewidth]{06MRI/phantom_cyclic.png}
    \caption{Zyklische Koordinaten-Updates}
    \label{fig:sfig3}
  \end{subfigure}%
  \caption{Rekonstruierte MRT-Bilder nach 100 Epochen mit den
           Parametern $\alpha=1, \eta=0.5, \gamma=0.5, \lambda=0.01$.
           Das Referenzbild wurde vom Matlab MRI package erzeugt.
           \ref{fig:sfig3} ist besser rekonstruiert als \ref{fig:sfig2}.}
\end{figure}

Als Input verwendet das paper die Matrix $A$ generiert durch Siddon's Algorithmus.
Das Referenzbild wird mit TVAL3 Paket \cite{TVAL3} erzeugt,
welches eine hohe Genauigkeit haben soll.
% Die Variablen $x, s, t$ werden entsprechend der Anzahl an Bildspalten in Blöcke aufgeteilt.
% Ein Koordinaten-Update aktualisiert $x, s, t$ gemeinsam, d.h. erst danach werden Zwischenwerte
% aktualisiert. Es ist anzumerken, dass dies
% im Gegensatz zur ursprünglichen Referenz des Algorithmus steht \cite{chow2016}.
% Dort enthält ein Update nur eine Variable $x, s oder t$.
Die Schrittweiten $\eta, \gamma$ des Primal-Dual-Splittings wurden empirisch bestimmt,
einmal für das vollständige Update
und einmal für die Koordinaten-Updates. Es werden keine expliziten Werte angegeben.
Außerdem ist $\alpha = 1$, da dies laut Autoren empirisch stets für die Konvergenz
gereicht hat, obwohl die Schrittweite von der Block-Anzahl abhängt, siehe auch
Theorem \ref{thm:konv}.
\\
Die reproduzierten Experimente verwenden als Input die vom MRI-Paket bereitgestellten
Parameter, inklusive Referenzbild. Wie in Kapitel \ref{sec:num} erwähnt mussten
die Schrittweiten $\eta, \gamma$ empirisch bestimmt werden. Im Gegensatz zum
paper verwenden alle Algorithmen die gleichen Werte, für eine bessere Vergleichbarkeit.
\\
Die reproduzierten Experimente konnten die Ergebnisse des papers bestätigen, wie 
beschrieben in Bild \ref{fig:epochs}.
Der Unterschied zwischen den einzelnen Methoden fällt jedoch geringer aus.

\section{Offene Fragen}

Da die Schrittweiten nicht angegeben werden, sind die zyklischen und das vollständige
Update nur schwer vergleichbar. Insbesondere da die Koordinaten-Updates höhere
Schrittweiten verwenden können, siehe \ref{eq:lip} oder auch \cite{Peng_2016}
Seite 6 für ein Beispiel. 
Ebenso ist kein repository angegeben um den verwendeten code nachvollziehen zu können.
\\
Die Konvergenz wird nur für Epochen betrachtet und nicht für die tatsächliche
Berechnungszeit. Das ist jedoch die relevante Information für praktische Anwendungen.
Zum Beispiel wird schon in der Einleitung des papers herausgehoben, dass die
Cache-Misses des zufälligen Koordinaten-Updates zu höheren Rechenzeiten führen können.
Die Berechnungszeit konnte ich mit Matlab nicht selbst testen, denn das zur Verfügung
gestellte Matlab-Paket läd die Transformationsmatrix $A$ als Funktionszeiger. Somit
lässt sich die ``caching''-Technik nicht mehr anwenden. Der nicht-optimierte
Algorithmus ist nicht mehr ``computationally worthy''.
\\
Greedy Koordinaten-Updates wurden nicht betrachtet. In \cite{nutini2015coordinate}
wird beispielsweise die Gauss-Southwell Regel für coordinate-descent Algorithmen untersucht.
Dort zeigt sich eine signifikant bessere Konvergenzrate gegenüber den zyklischen
Koordinaten-Updates.
\\
Die Autoren merken an, dass die Schrittweite invers proportional zur Anzahl der
Koordinaten ist. Experimentell hat aber stets eine Schrittweite von $1$ funktioniert.
Das lässt die Frage offen, ob die ermittelten Grenzwerte optimale Schranken sind,
oder ob unter weiteren Voraussetzungen bessere Grenzen erreicht werden können.
\\
Im paper wird angemerkt, dass shuffling die schlechtestmögliche Ordnung
verhindert und daher schneller konvergieren müsste als die zyklischen Updates. 
Durch die Experimente wird diese Aussage, aber nicht unterstützt.
\\
Der Fokus des papers liegt auf den sequentiellen Koordinaten-Updates.
Daher wäre es interessant in weiteren Veröffentlichungen die Ergebnisse auf
den async-parallelen Algorithmus zu verallgemeinern. Wie in der Einleitung
erwähnt, könnte so eine bessere Skalierbarkeit auf moderner Hardware erreicht
werden.