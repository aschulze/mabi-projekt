% Mathematical Imaging - practical project - André Schulze

% Implementation file for TV-Denoising with full and coordinate updates.
% The coordinate update has two implementations, a naive(default) one
% and a partially optimized one. Since the input matrix A is actually
% a function handle, the coordinate update could not be fully optimzed.
% It is therefore not computationally worthy.

function x = l1_tv(A, At, b, lambda, m, n, iter, eta, gamma, type)
% x = l1_tv(A, At, b, lambda, iter)
% Anisotropic total variation image denoising.
% Chow et al p15
% INPUT
%   A - transformation matrix (function handle)
%   At - transposed A (function handle)
%   b - observed data
%   lambda - regularization parameter
%   m - image rows
%   n - image columns
%   eta - step size
%   gamma - step size
%   iter - number of iterations
% OUTPUT
%   x - reconstructed image

    if type == update_t.full
        x = l1_tv_full(A, At, b, lambda, m, n, iter, eta, gamma);
    else
        % The naive implementation is used instead of the partially
        % optimized, since it seems to be faster in general.
        x = l1_tv_coordinates_naive(A, At, b, lambda, m, n, iter, eta, gamma, type);
    end
end

function x = l1_tv_full(A, At, b, lambda, m, n, iter, eta, gamma)
% x = l1_tv(A, At, b, lambda, iter)
% Anisotropic total variation image denoising.
% INPUT
%   A - transformation matrix (function handle)
%   At - transposed A (function handle)
%   b - observed data
%   lambda - regularization parameter
%   m - image rows
%   n - image columns
%   iter - number of iterations
%   eta - step size
%   gamma - step size
% OUTPUT
%   x - reconstructed image
    
    % discrete image gradient
    L = get_L(m, n);
    Lt = L.';
    nL = size(Lt, 2);
    
    % initial values
    x = zeros(m * n, 1);
    s = zeros(nL, 1);
    t = zeros(length(b), 1);
    
    for i = 1:iter
       c1 = eta * (Lt * s + At(t));
       c2 = x - 2 * c1;
       
       x = x - c1;
       s = max_proj(s + gamma * L * c2, lambda);
       t = (t + gamma * A(c2) - gamma * b) / (1 + gamma);   
    end
    
    x = reshape(x, m, n);
end

% Not in use because it is only partially optimized and slow. See above.
function x = l1_tv_coordinates(A, At, b, lambda, m, n, epochs, eta, gamma, type)
% x = l1_tv(A, At, b, lambda, iter)
% Anisotropic total variation image denoising using different types of
% coordinate updates.
% INPUT
%   A - transformation matrix (function handle)
%   At - transposed A (function handle)
%   b - observed data
%   lambda - regularization parameter
%   m - image rows
%   n - image columns
%   epochs - number of epochs
%   type - type of coordinate update (update_t enum)
%   eta - step size
%   gamma - step size
%   type- the type of coordinate update
% OUTPUT
%   x - reconstructed image
    
    % discrete image gradient
    L = get_L(m, n);
    Lt = L.';
    nL = size(Lt, 2);
    
    % initial values
    x = zeros(m * n, 1);
    s = zeros(nL, 1);
    t = zeros(length(b), 1);
    Lts = Lt * s;
    Att = At(t);

   % blocks = number of image columns
   % cave: size of t < block size is not checked
   s_t = fix(length(t) / n);
   
   if type == update_t.cyclic
       perm = linspace(1,n,n);
   elseif type == update_t.cyclic_shuffle
       perm = randperm(n);
   end

   % remark: The coordinate update is not fully optimized since A is a
   % function handle.
   for i = 1:epochs
       if type == update_t.random
           perm = randperm(n);
       end
       
       for j = 1:n
           s_ = s;
           c = x - 2 * eta * (Lts + Att);
           Ac = A(c);
           k = perm(j);

           idx1 = m*(k-1) + 1 : m*k;
           idx2 = idx1 + m*n;
           idx3 = s_t*(k-1) + 1 : min([s_t*k length(t)]);

           x(idx1) = x(idx1) - eta * (Lts(idx1) + Att(idx1));
           s(idx1) = max_proj(s(idx1) + gamma * L(k, :) * c, lambda);
           s(idx2) = max_proj(s(idx2) + gamma * L(m*n + k, :) * c, lambda);
           t(idx3) = (t(idx3) + gamma * Ac(idx3) - gamma * b(idx3)) / (1 + gamma);

           % update intermediate values at last
           Lts = Lts + L(k, :)*(s(idx1) - s_(idx1));
           Lts = Lts + L(k, :)*(s(idx2) - s_(idx2));
           Att = At(t);
       end
   end
   
   x = reshape(x, m, n);
end

function x = l1_tv_coordinates_naive(A, At, b, lambda, m, n, epochs, eta, gamma, type)
% x = l1_tv(A, At, b, lambda, iter)
% Anisotropic total variation image denoising using different types of
% coordinate updates. This is a naive implementation for reference/ testing.
% INPUT
%   A - transformation matrix (function handle)
%   At - transposed A (function handle)
%   b - observed data
%   lambda - regularization parameter
%   m - image rows
%   n - image columns
%   epochs - number of epochs
%   type - type of coordinate update (update_t enum)
%   eta - step size
%   gamma - step size
%   type- the type of coordinate update
% OUTPUT
%   x - reconstructed image
    
    % discrete image gradient
    L = get_L(m, n);
    Lt = L.';
    nL = size(Lt, 2);
    
    % initial values
    x = zeros(m * n, 1);
    s = zeros(nL, 1);
    t = zeros(length(b), 1);

   % blocks = number of image columns
   % cave: size of t < block size is not checked
   s_t = fix(length(t) / n);
   
   if type == update_t.cyclic
       perm = linspace(1,n,n);
   elseif type == update_t.cyclic_shuffle
       perm = randperm(n);
   end
   
   for i = 1:epochs
       if type == update_t.random
           perm = randperm(n);
       end
       
       for j = 1:n
           % recompute intermediate values after a (whole) block update
           c1 = eta * (Lt * s + At(t));
           c2 = x - 2 * c1;
           k = perm(j);
           
           idx1 = m*(k-1) + 1 : m*k;
           idx2 = idx1 + m*n;
           idx3 = s_t*(k-1) + 1 : min([s_t*k length(t)]);

           x_ = x - c1;
           s_ = max_proj(s + gamma * L * c2, lambda);
           t_ = (t + gamma * A(c2) - gamma * b) / (1 + gamma);  
           
           x(idx1) = x_(idx1);
           s(idx1) = s_(idx1);
           s(idx2) = s_(idx2);
           t(idx3) = t_(idx3);
       end
    end
   
   x = reshape(x, m, n);
end

function y = max_proj(x, lambda)
% y = max_proj(x, lambda)
% Orthogonal projection along the maximum norm.
% INPUT
%   x - data vector
%   lambda - threshold
% OUTPUT
%   y - maximum projection

    % written as moreau decomposition
    y = x - wthresh(x, 's', lambda);
end

% discrete image gradient
function L = get_L(m, n)
    % one-dimensional discrete derivative operator
    del_x = zeros(n, n);
    for i = 1:(n)
        for j = 1:(n)
            if i == j
                del_x(i, j) = -1;
            elseif j == i+1
                del_x(i, j) = 1;
            end
        end
    end
    
    del_y = zeros(m, m);
    for i = 1:(m)
        for j = 1:m
            if i == j
                del_y(i, j) = -1;
            elseif j == i+1
                del_y(i, j) = 1;
            end
        end
    end
    
    L = [ kron(sparse(del_x), speye(m));
          kron(speye(n), sparse(del_y)) ];
end