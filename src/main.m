% Mathematical Imaging - practical project - André Schulze

% main file - creating plots

% Load data.
numProjections = 20;
[A, At, b, u0] = mriLoadData( 'Logan', 2, numProjections, 0.0 );
n = size(u0, 1);      % quadratic image size n by n
m = length(b);        % number of measurements
lambda = 0.01;        % TV parameter

% step sizes for the full and cyclic updates (empiric)
eta_full = 0.5;
gamma_full = 0.5;
eta_cyclic = 0.5;
gamma_cyclic = 0.5;

% epochs = linspace(1, 50, 10);
epochs = [ 1 3 5 8 10 15 20 30 40 60 80 100];

errors_full = zeros(size(epochs));
errors_rnd = zeros(size(epochs));
errors_cyclic = zeros(size(epochs));
errors_shf = zeros(size(epochs));

norm_u0 = norm(u0);

for i = 1:length(epochs)
    % full update
    fn_full = @() l1_tv(A, At, b, lambda, n, n, epochs(i), ...
                        eta_full, gamma_full, update_t.full);
    errors_full(i) = norm(fn_full() - u0) / norm_u0;
    
    % random coordinate update
    fn_rnd = @() l1_tv(A, At, b, lambda, n, n, epochs(i), ...
                        eta_cyclic, gamma_cyclic, update_t.random);
    errors_rnd(i) = norm(fn_rnd() - u0) / norm_u0;
    
    % cyclic shuffle coordinate update
    fn_shf = @() l1_tv(A, At, b, lambda, n, n, epochs(i), ...
                          eta_cyclic, gamma_cyclic, update_t.cyclic_shuffle);
    errors_shf(i) = norm(feval(fn_shf) - u0) / norm_u0;
    
    % cyclic coordinate update
    fn_cyclic = @() l1_tv(A, At, b, lambda, n, n, epochs(i), ...
                          eta_cyclic, gamma_cyclic, update_t.cyclic);
    errors_cyclic(i) = norm(feval(fn_cyclic) - u0) / norm_u0;
end

subplot(2,2,1);
hold on
plot(epochs, errors_full, 'DisplayName', "full update")
plot(epochs, errors_rnd, 'DisplayName', "random coordinate update")
plot(epochs, errors_shf, 'DisplayName', "cyclic shuffle coordinate update")
plot(epochs, errors_cyclic, 'DisplayName', "cyclic coordinate update")
title("Primal Dual Splitting")
xlabel("Epoch")
ylabel("Relative L2 error")
hold off
legend

subplot(2,2,2);
imshow(u0);
subplot(2,2,3);
imshow(l1_tv(A, At, b, lambda, n, n, 100, eta_full, gamma_full, update_t.full));
subplot(2,2,4);
imshow(l1_tv(A, At, b, lambda, n, n, 100, eta_cyclic, gamma_cyclic, update_t.cyclic));