function [A, At, b, u_orig] = mriLoadData( dataset, instance, numProjections, noiseLevel )
%[A, At, b, u_orig] = mriLoadData( dataset, instance, numProjections, noiseLevel )
%
%   INPUT:
%   -------------------------------------------------------------
%   dataset           {'Batenburg','Logan', 'Weber', 'WeberMulti'}
%   instance          number of the instance in the dataset
%   numProjections    number of projections
%   noiseLevel        level of noise (typical 0.01)
%
%   OUTPUT:
%   --------------------------------------------------------------
%   A                 linear operator   Nxm
%   b                 observed data     mx1
%   u_orig            original image    WxH


    % Default value for noiseLevel
    if nargin < 4
        noiseLevel = 0.0;
    end
    
    % Default value for numProjections
    if nargin < 3
        numProjections = 16;
    end
    
    % Default value for instance
    if nargin < 2
        instance = 1;
    end
    
    % Default value for dataset
    if nargin < 1
        dataset = 'Logan';
    end

    % Add l1magic directory to path
    S = dbstack('-completenames');
    currentFolder = fileparts( S(1).file );
    addpath( [currentFolder, '/l1magic/Measurements'] )

    %% Load Data
    mfn  = mfilename;
    mffn = mfilename('fullpath');
    datapath = [mffn(1:end-numel(mfn)),'/data/',dataset,'/'];
    files    = dir([datapath,'*.p*']);
    if size(files,1) < 1
        error(['Error: no image data found for dataset ' dataset])
    end
    if instance < 1 || instance > size(files,1)
        error(['Error: instance must be in the range of [1,' num2str(size(files,1)) '] for dataset ' dataset])
    end
    
    u_orig = im2double(imread([datapath,files(instance).name]));
    u_orig = imresize(u_orig, [64,64]);
    
    if(numel(size(u_orig))==3)
        u_orig = squeeze(u_orig(:,:,1)); 
    end
    
    n = size(u_orig, 1);  
    u_orig(u_orig<0)=0;
    x = u_orig(:);

    % Fourier samples we are given
    [M,Mh,mh,mhi] = LineMask(numProjections,n);
    OMEGA = mhi;
    A = @(z) A_fhp(z, OMEGA);
    At = @(z) At_fhp(z, OMEGA, n);

    % measurements
    b = A(x);
   
    % Add noise
    if(noiseLevel > 0)
        Npts = length(b); % Number of input time samples
        Noise = poissrnd(b,Npts,1);%randn(Npts,1); % Generate initial noise; mean zero, variance one

        Signal_Power = sum(abs(b).*abs(b))/Npts;
        Noise_Power = sum(abs(Noise).*abs(Noise))/Npts;
        K = (Signal_Power/Noise_Power)*noiseLevel;   %10^(-Desired_SNR_dB/10);  % Scale factor

        New_Noise = sqrt(K)*Noise; 
        b = b + New_Noise;
    end