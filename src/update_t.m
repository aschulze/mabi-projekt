% Mathematical Imaging - practical project - André Schulze

classdef update_t
    % Class that defines update types for fixed point iterations.
    enumeration
        full, cyclic, cyclic_shuffle, random
    end
end